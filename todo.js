
 var listOfItems = document.querySelector("ul");
document.querySelector("button").addEventListener("click", function () {
  var inputText = document.querySelector(".my-text").value;
  var todoItem = document.createElement("li");
  var inputElement = document.createElement("input");
  inputElement.type = "checkbox";
  inputElement.addEventListener("change", function (event) {
    if (event.target.checked) {
      event.target.nextSibling.style.textDecoration = "line-through";

    }
    else {
      event.target.nextSibling.style.textDecoration = "none";
    }
  }, false);

  todoItem.appendChild(inputElement);
  let todoText = document.createElement("span");
  todoText.innerHTML = inputText;
  document.querySelector(".my-text").value = "";
  console.log(todoText);
  todoItem.appendChild(todoText);
  listOfItems.appendChild(todoItem);
  let closeButton = document.createElement("button");
  closeButton.textContent = "x";
  closeButton.style.fontSize = "10px";
  closeButton.addEventListener("click", function () {
    todoItem.remove();
  })
  todoItem.appendChild(closeButton);
})