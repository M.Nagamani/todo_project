let todoArray = [];
function addTodoItems() {
  var inputText = document.querySelector(".my-text").value;
  if (inputText === "") {
    alert("please enter the text first")
  }
  else {

    let todoObj = {};
    todoObj["text"] = inputText;
    todoObj["status"] = false;
    todoArray.push(todoObj);
    document.querySelector(".my-text").value = "";
    domRefresh();
    console.log(todoArray);
  }
}
function domRefresh() {

  let checklistRefresh = document.getElementById('items-list');
  while (checklistRefresh.firstChild) {
    checklistRefresh.removeChild(checklistRefresh.firstChild);
    console.log("refrshed");
  }
  todoArray.forEach((element) => {

    let todoItem = document.createElement("li");
    let todoText = document.createElement("span");
    todoText.textContent = element["text"];
    todoItem.append(todoText);
    var inputElement = document.createElement("input");
    inputElement.type = "checkbox";
    inputElement.className = "tick-Box";
    todoItem.prepend(inputElement);
    if (element["status"]) {
      inputElement.checked = true;
      inputElement.nextSibling.style.textDecoration = "line-through";
    }
    let closeButton = document.createElement("button");
    closeButton.textContent = "x";
    closeButton.style.fontSize = "10px";
    closeButton.className = "close";
    todoItem.append(closeButton);

    checklistRefresh.append(todoItem);

  })
}
document.addEventListener("click", function (event) {
  if (event.target.className == "add") {
    addTodoItems();
  }
  if (event.target.className == "close") {
    let parentItem = event.target.parentElement;
    todoArray.forEach(function (element, index, object) {
      if (element["text"] == parentItem.querySelector("span").innerText) {
        object.splice(index, 1)
        domRefresh();
      }
    })
  }
  if (event.target.className == "tick-Box") {
    let parentItem = event.target.parentElement;
    todoArray.forEach(function (element) {
      if (element["text"] == parentItem.querySelector("span").innerText) {
        if (element["status"] == false) {
          element["status"] = true;
        }
        else {
          element["status"] = false;
        }
        domRefresh()
      }
    })
  }

})